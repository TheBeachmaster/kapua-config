#! /bin/bash 

resp=$(curl -s --unix-socket /var/run/docker.sock http://ping > /dev/null)
status=$?

if ["$status" == "7" ]; then
    echo 'Docker does not seem to be up'
    exit 1
fi

echo 'Docker is Up,continuing...'

sudo docker stop $(sudo docker ps -a -q)

sudo docker rmi $(sudo docker images -a -q)

sudo docker system df

exit 0